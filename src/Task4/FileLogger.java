package Task4;

class FileLogger implements Logger {
    public void logInfo(String message) {
        // Implement the logic to write log message to a log file
        System.out.println("Writing INFO log to a log file: " + message);
    }

    public void logWarning(String message) {
        // Implement the logic to write log message to a log file
        System.out.println("Writing WARNING log to a log file: " + message);
    }

    public void logError(String message) {
        // Implement the logic to write log message to a log file
        System.out.println("Writing ERROR log to a log file: " + message);
    }
}