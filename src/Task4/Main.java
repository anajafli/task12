package Task4;

public class Main {
    public static void main(String[] args) {
        Logger consoleLogger = new ConsoleLogger();
        Logger fileLogger = new FileLogger();

        consoleLogger.logInfo("This is an information message");
        consoleLogger.logWarning("This is a warning message");
        consoleLogger.logError("This is an error message");

        System.out.println();

        fileLogger.logInfo("This is an information message");
        fileLogger.logWarning("This is a warning message");
        fileLogger.logError("This is an error message");
    }
}