package Task4;

class ConsoleLogger implements Logger {
    public void logInfo(String message) {
        System.out.println("INFO: " + message);
    }

    public void logWarning(String message) {
        System.out.println("WARNING: " + message);
    }

    public void logError(String message) {
        System.out.println("ERROR: " + message);
    }
}
