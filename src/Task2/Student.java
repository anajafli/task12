package Task2;

class Student extends Person {
    private int stId;

    public Student(String name, int age, int stId) {
        super(name, age);
        this.stId = stId;
    }

    public int getStId() {
        return stId;
    }

    public void setStId(int studentId) {
        this.stId = studentId;
    }

    public void displayInfo() {
        System.out.println("Student Name: " + getName());
        System.out.println("Student Age: " + getAge());
        System.out.println("Student ID: " + stId);
    }
}