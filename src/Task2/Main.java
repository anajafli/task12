package Task2;

public class Main {
    public static void main(String[] args) {
        Student student = new Student("Afat", 23, 6513);
        Teacher teacher = new Teacher("Faiq", 35, "QA");

        student.displayInfo();
        System.out.println();
        teacher.displayInfo();
    }
}
