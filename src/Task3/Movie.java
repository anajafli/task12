package Task3;

class Movie extends Media {
    public Movie(String title, int duration) {
        super(title, duration);
    }

    public void play() {
        System.out.println("Playing movie: " + getTitle());
    }
}
