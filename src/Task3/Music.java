package Task3;

class Music extends Media {
    public Music(String title, int duration) {
        super(title, duration);
    }

    public void play() {
        System.out.println("Playing music: " + getTitle());
    }
}