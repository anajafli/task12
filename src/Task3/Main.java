package Task3;

public class Main {
    public static void main(String[] args) {
        Music music = new Music("Azerbaijan song", 98);
        Movie movie = new Movie("My Mom", 30);

        music.play();
        movie.play();
    }
}